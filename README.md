# Company type client of [company type filler](https://devspace-com-ua.herokuapp.com/)

### Usage
```bash
dep ensure -add gitlab.com/qrsx/companytype-client
```

```golang
import typeclient "gitlab.com/qrsx/companytype-client"

companyTypeClient := typeclient.NewClient(&fasthttp.Client{}, "https://devspace-com-ua.herokuapp.com/state/result.json")

companyAliasTypeMap, err := companyTypeClient.CompanyAliasTypeMap()
```

### Types
```json
[
    {
        "alias": "product",
        "name": "Product"
    }, {
        "alias": "startup",
        "name": "Startup"
    }, {
        "alias": "outsource",
        "name": "Outsource"
    }, {
        "alias": "academy",
        "name": "Academy"
    }, {
        "alias": "recruitment-agency",
        "name": "Recruitment Agency"
    }
]
```