package client

// easyjson:json
type CompanyTypeState struct {
	CompanyAlias string `json:"company_alias"`
	TypeAlias    string `json:"type_alias"`
}

// easyjson:json
type CompanyTypeStateList []CompanyTypeState
