package client

import (
	"github.com/valyala/fasthttp"
)

type Client struct {
	httpClient *fasthttp.Client
	source     string
}

func NewClient(httpClient *fasthttp.Client, source string) *Client {
	return &Client{httpClient: httpClient, source: source}
}

func (c *Client) CompanyAliasTypeMap() (map[string]string, error) {
	request := fasthttp.AcquireRequest()
	response := fasthttp.AcquireResponse()

	defer func() {
		fasthttp.ReleaseRequest(request)
		fasthttp.ReleaseResponse(response)
	}()

	request.SetRequestURI(c.source)

	err := c.httpClient.Do(request, response)
	if err != nil {
		return nil, err
	}

	companyTypeStateList := make(CompanyTypeStateList, 0, 1024)

	err = companyTypeStateList.UnmarshalJSON(response.Body())
	if err != nil {
		return nil, err
	}

	result := make(map[string]string, len(companyTypeStateList))

	for _, companyTypeState := range companyTypeStateList {
		result[companyTypeState.CompanyAlias] = companyTypeState.TypeAlias
	}

	return result, nil
}
